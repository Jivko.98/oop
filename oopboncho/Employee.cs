﻿using System;
using System.Collections.Generic;
using System.Text;

namespace oopboncho
{
    public class Employee:Human
    {
        int employeeId;
      
        public Employee(string name, int age,int employeeId) : base(name, age)
        {
            this.EmployeeId = employeeId;
        }

        public int EmployeeId
        {
            get
            {
                return employeeId;
            }
          private  set
            {
                this.employeeId = value;
            }
        }
        public override void SayHi()
        {
            base.SayHi();
            Console.WriteLine("an Employee!");
        }
    }
}
