﻿using System;
using System.Collections.Generic;
using System.Text;

namespace oopboncho
{
   public class Human
    {
        private string name;
        private int age;

        public Human(string name,int age )
        {
            this.Name = name;
            this.Age = age;
        }

        public string Name
        {
            get
            {
                return this.name;
            }
          private  set
            {
                this.name = value;
            }
        }
        public int Age
        {
            get
            {
                return this.age;
            }
           private set
            {
                this.age = value;
            }
        }

       public virtual void SayHi()
        {
            Console.WriteLine($"Hi I am ");
            
        }

    }
}
