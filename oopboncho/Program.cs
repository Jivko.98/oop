﻿using System;
using System.Collections.Generic;

namespace oopboncho
{
    class Program
    {
        static void Main(string[] args)
        {
            string name;
            int age;
            int employeeId;
            int classId;
            Console.WriteLine("Hi welcome are you an employee or a student"+ Environment.NewLine+"1.Employee"+Environment.NewLine+"2.Student");
            
            List<Employee> employees = new List<Employee>();
            List<Student> students = new List<Student>();
            while (true)
            {
                string a = Console.ReadLine();
                if (a=="end")
                {
                    break;
                }
                switch (a)
                {
                    case "1":
                        Console.WriteLine("Name:");
                        name = Console.ReadLine();
                        Console.WriteLine("Age:");
                        age = int.Parse(Console.ReadLine());
                        Console.WriteLine("EmployeeId:");
                        employeeId = int.Parse(Console.ReadLine());
                        Employee employee = new Employee(name, age, employeeId);
                        employee.SayHi();
                        employees.Add(employee);
                        
                        break;
                    case "2":
                        Console.WriteLine("Name:");
                        name = Console.ReadLine();
                        Console.WriteLine("Age:");
                        age = int.Parse(Console.ReadLine());
                        Console.WriteLine("ClassId:");
                        classId = int.Parse(Console.ReadLine());
                        Student student = new Student(name, age, classId);
                        student.SayHi();
                        students.Add(student);
                        break;

                    default:
                        Console.WriteLine("Wrong input");
                        break;
                }
                Console.WriteLine("Choose 1 or 2 again or you can type end to end the program");
            }
            if (employees.Count!=0)
            {
                Console.WriteLine("Employess:");
                foreach (var emp in employees)
                {
                    Console.WriteLine($"{emp.Name} ,{emp.Age} years old with ID={emp.EmployeeId}");
                }
            }

            if (students.Count != 0)
            {
                Console.WriteLine("Students:");
                foreach (var stud in students)
                {
                    Console.WriteLine($"{stud.Name} ,{stud.Age} years old with classID={stud.ClassId}");
                }
            }
         
        }
    }
}
