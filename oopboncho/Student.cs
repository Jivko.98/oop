﻿using System;
using System.Collections.Generic;
using System.Text;

namespace oopboncho
{
    class Student : Human
    {
        int classId;
        
        public Student(string name, int age,int classId) : base(name, age)
        {
            this.ClassId = classId;
        }

        public int ClassId { get { return this.classId; } set { this.classId = value; } }

        public override void SayHi()
        {
            base.SayHi();
            Console.WriteLine("a Student! ");
        }
    }
}
